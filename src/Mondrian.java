import processing.core.PApplet;

public class Mondrian extends PApplet{

	private final int WIDTH = 800, HEIGHT = 800;
	
	public static void main(String[] args) {
		PApplet.main("Mondrian");
	}

	public void settings() {
		size(WIDTH, HEIGHT);
	}
	
	public void setup() {
		background(255);
		noLoop();
	}
	
	public void draw() {
		strokeWeight(0);
		monRekt(11, 0, 0, WIDTH, HEIGHT);
	}
	
	public void monRekt(int depth, float x, float y, float w, float h) {
		if(depth == 0) {
			rect(x, y, w, h);
		} else {
			// 1. Pick a random point in that rectangle
			// 2. Call monRekt() 4 times appropriately
			int i = (int)((Math.random()*w)+x);
			int j = (int)((Math.random()*h)+y);
			fill(255,0,0);
			monRekt(depth - 1, x, y, i - x, j - y);
			fill(255,255,255);
			monRekt(depth - 1, i, y, x + w - i, j - y);
			fill(0,0,200);
			monRekt(depth - 1, x, j, i - x, y + h - j);
			fill(255,255,170);
			monRekt(depth - 1, i, j, x + w - i, y + h - j);
		}
	}

}
